﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using IdeasRepositoryProject.Core.Entities;

namespace IdeasRepositoryProject.EFData.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            HasKey(h => h.Id);
            Property(h => h.Login).IsRequired().HasMaxLength(30);
            Property(h => h.Role).IsRequired();
            HasMany(h => h.Notes).WithRequired(h => h.Author).HasForeignKey(h=>h.UserId);
        }
    }
}
