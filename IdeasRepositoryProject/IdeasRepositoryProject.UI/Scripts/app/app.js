﻿function ViewModel() {
    var self = this;

    self.textBody = ko.observable("Bert");
    self.creationDate = ko.observable("Bert");
    self.author = ko.observable("Bert");

    self.editNote = function () {

    }

    self.updateName = function () {
        $.getJSON("http://localhost:3485/api/webapi/user", function (data) {
            var parsed = JSON.parse(data);
            
            self.firstName(parsed.firstName);
            self.lastName(parsed.lastName);
        });
    }

    self.addName = function () {
        var a = new ViewModel();
        a.textBody = self.textBody;
        a.creationDate = self.creationDate;
        a.author = self.author;

        $.ajax({
            type: "POST",
            url: "http://localhost:53407/api/ideasrepositoryapi/editnote",
            data: a,
            success: function(resultData) {
                alert(resultData);
            },
            dataType: JSON
        });
    }
}

ko.applyBindings(new ViewModel());

