﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IdeasRepositoryProject.BLLInterfaces.Exceptions
{
    public class UserServiceException : Exception
    {
        public UserServiceException()
        {

        }

        public UserServiceException(string message)
            : base(message)
        {

        }

        public UserServiceException(Exception exception)
            : base("Some exception occured", exception)
        {

        }
    }
}
