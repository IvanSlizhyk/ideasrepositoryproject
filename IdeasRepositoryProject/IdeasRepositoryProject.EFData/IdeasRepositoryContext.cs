﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using IdeasRepositoryProject.Core.Entities;
using IdeasRepositoryProject.EFData.Mapping;

namespace IdeasRepositoryProject.EFData
{
    public class IdeasRepositoryContext : DbContext
    {
        public DbSet<Note> Notes { get; set; }
        public DbSet<User> Users { get; set; }

        public IdeasRepositoryContext(string connectionStringName)
            : base(connectionStringName)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new NoteMap());
            modelBuilder.Configurations.Add(new UserMap());
            
            base.OnModelCreating(modelBuilder);
        }
    }
}
