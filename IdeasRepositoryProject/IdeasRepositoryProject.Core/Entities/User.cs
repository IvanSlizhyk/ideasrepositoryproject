﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IdeasRepositoryProject.Core.Entities
{
    public class User : BaseEntity<int>
    {
        public virtual List<Note> Notes { get; set; }
        public string Login { get; set; }
        public Role Role { get; set; }
    }
}
