﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using IdeasRepositoryProject.Core.Entities;

namespace IdeasRepositoryProject.UI.Models
{
    [DataContract]
    public class NoteViewModel
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }
        [DataMember(Name = "textBody")]
        public string TextBody { get; set; }
        [DataMember(Name = "creationDate")]
        public DateTime CreationDate { get; set; }
        [DataMember(Name = "author")]
        public User Author { get; set; };
    }
}