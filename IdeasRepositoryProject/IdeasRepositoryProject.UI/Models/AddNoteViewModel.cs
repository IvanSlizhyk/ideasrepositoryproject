﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IdeasRepositoryProject.UI.Models
{
    [DataContract]
    public class AddNoteViewModel
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }
        [DataMember(Name = "textBody")]
        public string TextBody { get; set; }
    }
}