﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IdeasRepositoryProject.Core.Entities;

namespace IdeasRepositoryProject.DALInterfaces
{
    public interface IRepositoryFactory
    {
        IRepositoryGeneric<User, int> GetUserRepository();
        IRepositoryGeneric<Note, int> GetNoteRepository();
    }
}
