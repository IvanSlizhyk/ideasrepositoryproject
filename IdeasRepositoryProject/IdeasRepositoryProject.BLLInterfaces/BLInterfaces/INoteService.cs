﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IdeasRepositoryProject.Core.Entities;

namespace IdeasRepositoryProject.BLLInterfaces.BLInterfaces
{
    public interface INoteService : IService
    {
        Note CreateNote(User user, Status status, string textBody);
        void RemoveNote(Note note);
        void UpdateNote(Note note);
        Note GetNoteById(int noteId);
        List<Note> GetAllNotes();
    }
}
