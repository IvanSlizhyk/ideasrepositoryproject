﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IdeasRepositoryProject.Core.Entities
{
    public class BaseEntity<T>
    {
        public int Id { get; set; }
    }
}
