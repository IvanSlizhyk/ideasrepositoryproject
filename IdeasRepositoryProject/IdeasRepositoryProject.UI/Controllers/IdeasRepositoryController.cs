﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IdeasRepositoryProject.Core.Entities;
using IdeasRepositoryProject.EFData;
using IdeasRepositoryProject.Services.Services;
using WebMatrix.WebData;

namespace IdeasRepositoryProject.UI.Controllers
{
    public class IdeasRepositoryController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Home()
        {
            var context = new IdeasRepositoryContext(Resources.connectionStringName);
            var unit = new UnitOfWork(context);
            var repositoryFactory = new RepositoryFactory(context);
            var userService = new UserService(unit, repositoryFactory);
            var user = userService.GetUserByLogin(User.Identity.Name);

            if (user.Role == Role.Admin)
            {
                return RedirectToAction("AdminNotes", "IdeasRepository");
            }
            return RedirectToAction("UserNotes", "IdeasRepository");
        }

        [HttpGet]
        public ActionResult UserNotes()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AdminNotes()
        {
            return View();
        }

        [HttpGet]
        public ActionResult EditNote()
        {
            return View();
        }

    }
}
