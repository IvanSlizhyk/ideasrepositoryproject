﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IdeasRepositoryProject.Core.Entities
{
    public enum Status
    {
        Posted = 0,
        Deleted = 1
    }
}
