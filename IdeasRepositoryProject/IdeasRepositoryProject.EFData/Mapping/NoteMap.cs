﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using IdeasRepositoryProject.Core.Entities;

namespace IdeasRepositoryProject.EFData.Mapping
{
    public class NoteMap : EntityTypeConfiguration<Note>
    {
        public NoteMap()
        {
            HasKey(h => h.Id);
            Property(h => h.CreationDate).IsRequired();
            Property(h => h.Status).IsRequired();
            Property(h => h.TextBody).IsRequired().HasMaxLength(2000);
            HasRequired(h => h.Author).WithMany(h => h.Notes).HasForeignKey(h=>h.UserId);
        }
    }
}
