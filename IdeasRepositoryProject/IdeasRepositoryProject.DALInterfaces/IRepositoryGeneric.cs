﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using IdeasRepositoryProject.Core.Entities;

namespace IdeasRepositoryProject.DALInterfaces
{
    public interface IRepositoryGeneric<TEntity, in TKey> : IBaseRepository where TEntity : BaseEntity<TKey>
    {
        void Create(TEntity value);
        void Update(TEntity value);
        void Remove(TEntity value);
        TEntity GetEntityById(TKey id);
        TEntity FindEntity(Expression<Func<TEntity, bool>> predicate);
        IQueryable<TEntity> All();
        IQueryable<TEntity> FindEntities(Expression<Func<TEntity, bool>> predicate);
    }
}
