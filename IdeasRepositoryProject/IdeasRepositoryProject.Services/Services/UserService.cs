﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IdeasRepositoryProject.BLLInterfaces.BLInterfaces;
using IdeasRepositoryProject.BLLInterfaces.Exceptions;
using IdeasRepositoryProject.Core.Entities;
using IdeasRepositoryProject.DALInterfaces;

namespace IdeasRepositoryProject.Services.Services
{
    public class UserService : BaseService, IUserService
    {
        public UserService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {
        }

        public User CreateUser(string login, Role role)
        {
            var userRepository = RepositoryFactory.GetUserRepository();
            var user = new User(){Login = login, Role = role};
            userRepository.Create(user);

            try
            {
                UnitOfWork.Save();
            }
            catch (UserServiceException e)
            {
                throw new UserServiceException(e);
            }
            return user;
        }

        public void RemoveUser(User user)
        {
            var userRepository = RepositoryFactory.GetUserRepository();

            try
            {
                userRepository.Remove(user);
            }
            catch (UserServiceException e)
            {
                throw new UserServiceException(e);
            }
        }

        public void UpdateUser(User user)
        {
            var userRepository = RepositoryFactory.GetUserRepository();

            try
            {
                userRepository.Update(user);
            }
            catch (UserServiceException e)
            {
                throw new UserServiceException(e);
            }
        }

        public User GetUserByLogin(string login)
        {
            var userRepository = RepositoryFactory.GetUserRepository();

            try
            {
                var user = userRepository.FindEntity(h => h.Login == login);
                return user;
            }
            catch (UserServiceException e)
            {
                throw new UserServiceException(e);
            }
        }

        public List<User> GetAllUsers()
        {
            var userRepository = RepositoryFactory.GetUserRepository();

            try
            {
                var users = userRepository.All().ToList();
                return users;
            }
            catch (UserServiceException e)
            {
                throw new UserServiceException(e);
            }
        }
    }
}
