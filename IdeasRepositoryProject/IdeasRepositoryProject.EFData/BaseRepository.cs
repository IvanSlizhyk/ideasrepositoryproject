﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IdeasRepositoryProject.EFData
{
    public class BaseRepository
    {
        protected IdeasRepositoryContext Context { get; set; }

        public BaseRepository(IdeasRepositoryContext context)
        {
            Context = context;
        }
    }
}
