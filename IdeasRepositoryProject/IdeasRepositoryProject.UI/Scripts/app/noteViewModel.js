﻿function AppViewModel() {
    var self = this;

    self.notes = ko.observableArray([
    { author: "111", creationDate: "222", textBody: "333"}
    ]);

    self.addNote = function () {
        var a = new AppViewModel();
        a.notes = self.notes;

        $.ajax({
            type: "POST",
            url: "http://localhost:53407/api/ideasrepositoryapi/addnote",
            data: a,
            success: function (resultData) {
                alert(resultData);
                self.notes.push({ author: resultData.author, creationDate: resultData.creationDate, textBody: resultData.textBody });
            },
            dataType: JSON
        });
    };

    self.removeNote = function () {
        self.notes.remove(this);
    }
}

ko.applyBindings(new AppViewModel());

