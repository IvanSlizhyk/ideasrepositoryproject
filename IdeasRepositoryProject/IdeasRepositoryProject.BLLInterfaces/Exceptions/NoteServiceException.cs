﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IdeasRepositoryProject.BLLInterfaces.Exceptions
{
    public class NoteServiceException : Exception
    {
        public NoteServiceException()
        {

        }

        public NoteServiceException(string message)
            : base(message)
        {

        }

        public NoteServiceException(Exception exception)
            : base("Some exception occured", exception)
        {

        }
    }
}
