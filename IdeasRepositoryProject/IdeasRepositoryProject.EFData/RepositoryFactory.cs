﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IdeasRepositoryProject.Core.Entities;
using IdeasRepositoryProject.DALInterfaces;

namespace IdeasRepositoryProject.EFData
{
    public class RepositoryFactory : IRepositoryFactory
    {
        private readonly IdeasRepositoryContext _context;
        private IRepositoryGeneric<User, int> _userRepository;
        private IRepositoryGeneric<Note, int> _noteRepository;

        public RepositoryFactory(IdeasRepositoryContext context)
        {
            _context = context;
        }

        public IRepositoryGeneric<User, int> GetUserRepository()
        {
            return _userRepository ?? (_userRepository = new RepositoryGeneric<User, int>(_context));
        }

        public IRepositoryGeneric<Note, int> GetNoteRepository()
        {
            return _noteRepository ?? (_noteRepository = new RepositoryGeneric<Note, int>(_context));
        }
    }
}
