﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IdeasRepositoryProject.Core.Entities;

namespace IdeasRepositoryProject.BLLInterfaces.BLInterfaces
{
    public interface IUserService : IService
    {
        User CreateUser(string login, Role role);
        void RemoveUser(User user);
        void UpdateUser(User user);
        User GetUserByLogin(string login);
        List<User> GetAllUsers();
    }
}
