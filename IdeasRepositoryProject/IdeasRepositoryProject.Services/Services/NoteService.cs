﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IdeasRepositoryProject.BLLInterfaces.BLInterfaces;
using IdeasRepositoryProject.BLLInterfaces.Exceptions;
using IdeasRepositoryProject.Core.Entities;
using IdeasRepositoryProject.DALInterfaces;

namespace IdeasRepositoryProject.Services.Services
{
    public class NoteService : BaseService, INoteService
    {
        public NoteService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {
        }
        
        public Note CreateNote(User user, Status status, string textBody)
        {
            var noteRepository = RepositoryFactory.GetNoteRepository();
            var note = new Note(){CreationDate = DateTime.Now, Status = status, TextBody = textBody, Author = user, UserId = user.Id};
            noteRepository.Create(note);

            try
            {
                UnitOfWork.Save();
            }
            catch (NoteServiceException e)
            {
                throw new NoteServiceException(e);
            }
            return note;
        }

        public void RemoveNote(Note note)
        {
            var noteRepository = RepositoryFactory.GetNoteRepository();

            try
            {
                noteRepository.Remove(note);
            }
            catch (NoteServiceException e)
            {
                throw new NoteServiceException(e);
            }
        }

        public void UpdateNote(Note note)
        {
            var noteRepository = RepositoryFactory.GetNoteRepository();

            try
            {
                noteRepository.Update(note);
            }
            catch (NoteServiceException e)
            {
                throw new NoteServiceException(e);
            }
        }

        public Note GetNoteById(int noteId)
        {
            var noteRepository = RepositoryFactory.GetNoteRepository();

            try
            {
                var note = noteRepository.GetEntityById(noteId);
                return note;
            }
            catch (NoteServiceException ex)
            {
                throw new NoteServiceException(ex);
            }
        }

        public List<Note> GetAllNotes()
        {
            var noteRepository = RepositoryFactory.GetNoteRepository();

            try
            {
                var notes = noteRepository.All();
                return notes.ToList();
            }
            catch (NoteServiceException ex)
            {
                throw new NoteServiceException(ex);
            }
        }
    }
}