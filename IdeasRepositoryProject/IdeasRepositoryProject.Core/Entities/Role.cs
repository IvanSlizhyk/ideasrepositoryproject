﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IdeasRepositoryProject.Core.Entities
{
    public enum Role
    {
        Admin = 0,
        User = 1
    }
}
