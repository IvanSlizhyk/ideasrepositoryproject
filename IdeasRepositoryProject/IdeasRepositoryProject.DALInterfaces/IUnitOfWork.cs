﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IdeasRepositoryProject.DALInterfaces
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
        void Rollback();
        void Save();
    }
}
