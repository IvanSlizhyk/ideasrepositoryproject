﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IdeasRepositoryProject.Core.Entities;
using IdeasRepositoryProject.EFData;
using IdeasRepositoryProject.Services.Services;
using IdeasRepositoryProject.UI.Models;
using Newtonsoft.Json;

namespace IdeasRepositoryProject.UI.Controllers
{
    public class IdeasRepositoryApiController : ApiController
    {
        [HttpPost]
        public string AddNote(AddNoteViewModel noteView)
        {
            var context = new IdeasRepositoryContext(Resources.connectionStringName);
            var unit = new UnitOfWork(context);
            var repositoryFactory = new RepositoryFactory(context);
            var userService = new UserService(unit, repositoryFactory);
            var noteService = new NoteService(unit, repositoryFactory);

            var user = userService.GetUserByLogin(User.Identity.Name);
            var note = noteService.CreateNote(user, Status.Posted, noteView.TextBody);

            return JsonConvert.SerializeObject(note);
        }
        
    }
}
