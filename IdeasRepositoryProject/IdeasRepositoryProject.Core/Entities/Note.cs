﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IdeasRepositoryProject.Core.Entities
{
    public class Note : BaseEntity<int>
    {
        public DateTime CreationDate { get; set; }
        public virtual User Author { get; set; }
        public int UserId { get; set; }
        public string TextBody { get; set; }
        public Status Status { get; set; }
    }
}
